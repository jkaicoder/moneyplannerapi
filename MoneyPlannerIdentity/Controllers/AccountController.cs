﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MoneyPlanner;
using MoneyPlannerIdentity.Models;
using MoneyPlannerIdentity.ViewModel;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MoneyPlannerIdentity.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private UserManager<User> UserManager { get; }

        public AccountController(UserManager<User> userManager)
        {
            UserManager = userManager;
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody] RegisterViewModel registerViewModel)
        {
            var user = new User(){ Email = registerViewModel.Email, UserName = registerViewModel.Password};
            var result = await UserManager.CreateAsync(user, registerViewModel.Password);

            if (result.Succeeded)
            {
                return this.OkResult();
            }
            else
            {
                if (result.Errors.Any(x=> x.Code == "DuplicateUserName"))
                {
                    return this.ErrorResult(ErrorCode.ACCOUNT_DUPPLICATE_USERNAME);
                }

                return this.ErrorResult(ErrorCode.BAD_REQUEST);
            }
               
        }
    }
}
