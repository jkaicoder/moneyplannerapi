﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoneyPlanner
{
    public enum ErrorCode
    {
        ACCOUNT_DUPPLICATE_USERNAME = 1001,
        BAD_REQUEST = 9999
    }
}
